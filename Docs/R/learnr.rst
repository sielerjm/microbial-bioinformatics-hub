Learning R
==========

.. dropdown:: :fa:`eye,mr-1` tl;dr
   :title: text-info

   R is a free, open-source programming language used often for statistical analyses. R can be run on the command line or using a graphical user interface. Below you'll find some helpful resources and :ref:`tutorials` for how to use R.

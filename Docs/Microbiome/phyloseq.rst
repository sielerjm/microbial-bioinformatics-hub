Phyloseq
========


.. dropdown:: :fa:`eye,mr-1` tl;dr
   :title: text-info

   Phyloseq is a software package in R that takes in data containing sample information, taxonomic classifications and operational taxanomic unit (OTU) counts. These data can be used for statistical analysis and data visualization. Using Phyloseq is relatively straight forward, but the package does assume a basic familiarity with using R and running commands on a terminal. Below you'll find some helpful :ref:`tutorials` for how to use phyloseq.


Introduction
------------

.. (Include the most relevant information for someone who has never heard of this software before. The main takeaway should be whether or not this tool will be useful for them or if they need to look for something else)


What is Phyloseq?
^^^^^^^^^^^^^^^^^

Phyloseq is an R package for reproducible interactive analysis and graphics of microbiome census data and was developed by Paul McMurdie and Susan Holmes in 2013. Quoting from the Phyloseq `website <https://joey711.github.io/phyloseq/index.html>`_:

   *The phyloseq package is a tool to import, store, analyze, and graphically display complex phylogenetic sequencing data that has already been clustered into Operational Taxonomic Units (OTUs), especially when there is associated sample data, phylogenetic tree, and/or taxonomic assignment of the OTUs. This package leverages many of the tools available in R for ecology and phylogenetic analysis (vegan, ade4, ape, picante), while also using advanced/flexible graphic systems (ggplot2) to easily produce publication-quality graphics of complex phylogenetic data. phyloseq uses a specialized system of S4 classes to store all related phylogenetic sequencing data as single experiment-level object, making it easier to share data and reproduce analyses. In general, phyloseq seeks to facilitate the use of R for efficient interactive and reproducible analysis of OTU-clustered high-throughput phylogenetic sequencing data.*


Why use Phyloseq?
^^^^^^^^^^^^^^^^^

You've generated OTU/ASV data and you want to analyze and create publication quality figures.


How does Phyloseq work?
^^^^^^^^^^^^^^^^^^^^^^^

Quoting from the Phyloseq paper [1]_:

   *The phyloseq package provides an object-oriented programming infrastructure that simplifies many of the common data management and preprocessing tasks required during analysis of phylogenetic sequencing data. This simplified syntax helps mitigate inconsistency errors and encourages interaction with the data during preprocessing. The phyloseq package also provides a set of powerful analysis and graphics functions, building upon related packages available in R and Bioconductor. It includes or supports some of the most commonly-needed ecology and phylogenetic tools, including a consistent interface for calculating ecological distances and performing dimensional reduction (ordination). The graphics functions allow users to interactively produce annotated publication-quality graphics in just one or two lines of code.*

In other words, Phyloseq simplifies the process for analyzing and visualizing microbial bioinformatic data.


What's needed to use Phyloseq?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Technical
   * Windows, MacOS, or Linux
   * R or Rstudio
   * `Installation of Phyloseq <https://joey711.github.io/phyloseq/install.html>`_
   * `Installation of ggplot2 <https://joey711.github.io/phyloseq/install.html>`_
* Knowledge
   * Basic familiarity with R. If you need to freshen up on your R skills, check out the R Basics page (*coming soon*)
   * Basic understanding of microbial bioinformatics
   * Basic understanding of statistical tests
* Data
   * Import abundance and related data from popular Denoising / OTU-clustering pipelines: (DADA2, UPARSE, QIIME, mothur, BIOM, PyroTagger, RDP, etc.)
      * Sample metadata
      * Taxanomic classification of microbial data
      * OTU/ASV counts
      * Phylogenomic data (optional)


.. _tutorials:

Tutorials
---------

Below are some of our favorite Phyloseq with a brief description of what makes them standout:

1. `The official Phyloseq tutorial <https://joey711.github.io/phyloseq/import-data.html>`_

   * This is a step-by-step walkthrough written by the developer of Phyloseq. There are additional tutorials for different visualization methods.
      * `Frequently Asked Questions <https://www.bioconductor.org/packages/release/bioc/vignettes/phyloseq/inst/doc/phyloseq-FAQ.html>`_: Answers common questions such as "How can I modify the plots?" and "How should I normalize my data?"
   * Highly recommended for first-time users.

2. `Vaulot Phyloseq Tutorial <https://vaulot.github.io/tutorials/Phyloseq_tutorial.html>`_

   * This is a good tutorial for beginners and demonstrates some slightly different ways of using phyloseq than the official package, but it is very similar. It does not go into as much depth about what each of the function options do, so for that reason we recommend you try this tutorial after you've successfully completed Phyloseq's tutorial.

3. `Bioconductor workflow for microbiome data analysis: from raw reads to community analyses <https://f1000research.com/articles/5-1492/v1>`_

   * This is a much more in-depth tutorial starting with importing data from DADA2 and proceeding through statistical analyses in Phyloseq. From the paper:
      *In this paper, we show that statistical models allow more accurate abundance estimates. By providing a complete workflow in R, we enable the user to do sophisticated downstream statistical analyses, including both parameteric and nonparametric methods. We provide examples of using the R packages dada2, phyloseq, DESeq2, ggplot2 and vegan to filter, visualize and test microbiome data. We also provide examples of supervised analyses using random forests, partial least squares and linear models as well as nonparametric testing using community networks and the ggnetwork package.*
   * We recommend this tutorial after you've feel comfortable with the basics of phyloseq.

What next?
----------
   * Differential abundance analysis (*coming soon...*)
   * Effect size analysis (*coming soon...*)
   * Phylogenomic analysis (*coming soon...*)


Alternatives
------------

None that we are aware of that is analogous to Phyloseq.


.. dropdown:: :fa:`eye,mr-1` References
   :title: text-info

      .. [1] `phyloseq: An R Package for Reproducible Interactive Analysis and Graphics of Microbiome Census Data <https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0061217/>`_

Getting Started
===============

Microbial bioinformatics is a rapidly evolving field. No longer at the whims of culturing microbes, microbial bioinformaticians can run their experiments as fast as their computational power allows them. While this has led to the development of new tools and insights into our microbial world and human health, it can be a difficult field to break into if you aren't familiar with the ins and outs of computer programming. The major goal of this site is to improve accessibility of bioinformatics to people of any skill level. Whether you're an early career scientist just getting started learning about bioinformatics or a seasoned professional, hopefully we'll have something that everyone can learn on your journey of becoming a more proficient bioinformatician.

.. toctree::
   :maxdepth: 2

   /Microbiome/beginners_guide_bioinformatics.rst


Collaboration
-------------

Microbial Bioinformatics Hub is a collaborative project. Each of us can't know every concept, method or tool related to microbial bioinformatics. Therefore, the success of this project depends on community collaboration. If you have  expertise in a particular subdomain of microbial bioinformatics, a really nice pipeline for analyzing metagenomes, or maybe you aren't quite there yet but are noticing all the grammar mistakes we're making. Whatever it may be, we would be happy to collaborate. One of the exciting things about the platform this site is running on is that collaboration is made incredibly easy. To find out how you can contribute, check out the links below:

.. toctree::
   :maxdepth: 1

   /Git/collab_with_git.rst
   /Git/collab_without_git.rst


.. figure:: ../Media/Images/Design/Getting_Started/Collaboration.png
   :scale: 33 %
   :align: center
   :alt: Everyone is an expert about something

   Each of us are experts in something [1]_.


.. dropdown:: :fa:`eye,mr-1` References
   :title: text-info

      .. [1] Adapted from `Agile Coffee <http://agilecoffee.com/toolkit/imposter-syndrome/>`_.
